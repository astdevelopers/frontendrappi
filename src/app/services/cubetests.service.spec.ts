import { TestBed, inject } from '@angular/core/testing';

import { CubetestsService } from './cubetests.service';

describe('CubetestsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CubetestsService]
    });
  });

  it('should be created', inject([CubetestsService], (service: CubetestsService) => {
    expect(service).toBeTruthy();
  }));
});

import { TestBed, inject } from '@angular/core/testing';

import { CubepropsService } from './cubeprops.service';

describe('CubepropsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CubepropsService]
    });
  });

  it('should be created', inject([CubepropsService], (service: CubepropsService) => {
    expect(service).toBeTruthy();
  }));
});

import {environment} from '../../environments/environment';
import { BasicResponse } from '../models/basic.response';
import {CubeProps} from '../models/cube.props';
import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CubepropsService {

  private loginUrl = environment.apriUrl + environment.cubePropsApiUrl;
  private basicResponse: BasicResponse;

  constructor(private http:  HttpClient) {}

  getAllCubeProps(): Promise<CubeProps[]> {
   return this.http.get<BasicResponse>(this.loginUrl )
     .toPromise().then(basicResponse => basicResponse.data as CubeProps[])
     .catch(this.handleError);
  }

  saveCubeProps(cubeProps: CubeProps): Promise<CubeProps> {
    return this.http.post<BasicResponse>(this.loginUrl, cubeProps, httpOptions )
      .toPromise()
      .then(basicResponse => basicResponse.data as CubeProps)
      .catch(this.handleError);
  }

  getcubePropsById(cubeProps: CubeProps): Promise<CubeProps> {
    return this.http.get<CubeProps>(this.loginUrl + cubeProps.id + '/')
      .toPromise()
      .then(response => response as CubeProps)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}

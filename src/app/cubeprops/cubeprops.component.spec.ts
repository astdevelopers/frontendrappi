import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubepropsComponent } from './cubeprops.component';

describe('CubepropsComponent', () => {
  let component: CubepropsComponent;
  let fixture: ComponentFixture<CubepropsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubepropsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubepropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

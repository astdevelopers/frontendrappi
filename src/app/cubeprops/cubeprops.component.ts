import {Component, OnInit} from '@angular/core';

import {CubeProps} from '../models/cube.props';
import {CubepropsService} from '../services/cubeprops.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Logger} from 'angular2-logger/core';


@Component({
  selector: 'app-cubeprops',
  templateUrl: './cubeprops.component.html',
  styleUrls: ['./cubeprops.component.css'],
  providers: [CubepropsService]
})
export class CubepropsComponent implements OnInit {
  [x: string]: any;
  public cubePropses: CubeProps[];
  public cubeProps: CubeProps = new PrimeCubeProps();

  options: FormGroup;

  ngOnInit(): void {
    // this.logger.debug('ngOnInit');
  }

  constructor(fb: FormBuilder, private logger: Logger, private _cubepropsService: CubepropsService) {
    this.options = fb.group({
      'amountOperations': [1, Validators.compose([Validators.max(1000), Validators.min(1)])],
      'matrixsize': [1, Validators.compose([Validators.max(100), Validators.min(1)])],
      'amountTest': [1, Validators.compose([Validators.max(50), Validators.min(1)])],
    });
    this.setCubeProps();
  }

  setCubeProps() {
    if (!this.cubeProps || !this.cubeProps.amountTest) {
      this.cubeProps.amountOperations = 0;
      this.cubeProps.amountTest = 0;
      this.cubeProps.matrixSize = 0;
    }
  }

  public save() {
    this.logger.info(this._cubepropsService);
    this._cubepropsService.saveCubeProps(this.cubeProps).then(cubeProps => {
      this.getAllUser();
      this.logger.info(cubeProps);
      this.logger.info('test successfully');
    });
  }

  public getAllUser() {
    this._cubepropsService.getAllCubeProps().then(user => {
      this.logger.info('test successfully');
    });
  }

}

class PrimeCubeProps implements CubeProps {
  constructor(
    public id?,
    public amountTest?,
    public matrixSize?,
    public amountOperations?) {}
}



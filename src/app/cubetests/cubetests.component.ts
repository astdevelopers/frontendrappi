import { CubeProps } from '../models/cube.props';
import { CubepropsService } from '../services/cubeprops.service';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Logger } from 'angular2-logger/core';

@Component({
  selector: 'app-cubetests',
  templateUrl: './cubetests.component.html',
  styleUrls: ['./cubetests.component.css'],
  providers: [CubepropsService]
})
export class CubetestsComponent implements OnInit {
  cubeTest: FormGroup;
  public cubePropses: CubeProps[];
  public cubeProps: CubeProps = new PrimeCubeProps();

  ngOnInit(): void {
    // throw new Error('Method not implemented.');
  }

  constructor(fb: FormBuilder, private _cubepropsService: CubepropsService, private logger: Logger) {
     this.cubeTest = fb.group({
       'cubePropertiestest': [1, Validators.required],
      'amountOperations': [1, Validators.compose([Validators.max(1000), Validators.min(1)])],
      'matrixsize': [1, Validators.compose([Validators.max(100), Validators.min(1)])],
      'amountTest': [1, Validators.compose([Validators.max(50), Validators.min(1)])],
    });
    this.getAllUser();
  }

   public getAllUser() {
     this._cubepropsService.getAllCubeProps().then(cubePropses => this.cubePropses = cubePropses);
     console.log(this.cubePropses);
  }

}


class PrimeCubeProps implements CubeProps {
  constructor(
    public id?,
    public amountTest?,
    public matrixSize?,
    public amountOperations?) {}
}

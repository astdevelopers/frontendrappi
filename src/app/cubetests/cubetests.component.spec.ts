import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubetestsComponent } from './cubetests.component';

describe('CubetestsComponent', () => {
  let component: CubetestsComponent;
  let fixture: ComponentFixture<CubetestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubetestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubetestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {BrowserModule} from '@angular/platform-browser';
import {NgModule, isDevMode} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Logger} from 'angular2-logger/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';
import {HttpClientModule} from '@angular/common/http';



import {AppComponent} from './app.component';
import {AngularMaterialModule} from './angularmaterials/angularmaterials.module';
import {CubepropsComponent} from './cubeprops/cubeprops.component';
import {CubetestsComponent} from './cubetests/cubetests.component';



@NgModule({
  declarations: [
    AppComponent,
    CubepropsComponent,
    CubetestsComponent
  ],
  imports: [
    BrowserModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [Logger],
  bootstrap: [AppComponent]
})
export class AppModule {
   constructor(private logger: Logger) {
    if (isDevMode()) {
      console.info('To see debug logs enter: \'logger.level = logger.Level.DEBUG;\' in your browser console');
    }
    this.logger.level = environment.logger.level;
  }
}

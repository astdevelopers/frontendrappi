import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  tiles = [
    {text: '<app-cubeprops></app-cubeprops>', cols: 1, rows: 1 },
    {text: '<app-cubeprops></app-cubeprops>', cols: 1, rows: 1},
    {text: '<app-cubeprops></app-cubeprops>', cols: 1, rows: 1},
  ];

}
